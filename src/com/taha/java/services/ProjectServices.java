package com.taha.java.services;

import java.util.Set;
import java.util.Vector;

import com.taha.java.models.ClassModel;
import com.taha.java.models.PackageModel;
import com.taha.java.models.ProjectModel;
import com.taha.java.models.Relation;
import com.taha.java.reflection.PackageExplorer;
import com.taha.java.reflection.ProjectExplorer;
import com.taha.java.reflection.ProjectRelationsExplorer;
import com.taha.java.xml.ProjectSaxParser;

import static com.taha.java.utils.StringUtil.getLastStringAfterSequence;
import static com.taha.java.utils.FileHandler.saveContentAsXml;

public class ProjectServices {

	private static final String XML_PROJECTS = "./resources/projects/xml/";
	private PackageExplorer pckgExplorer;
	private ProjectExplorer projectExplorer;
	private String projectPath;
	private ProjectModel project;
	
	public ProjectServices(String projectPath) {
		this.projectPath = projectPath;
		pckgExplorer = new PackageExplorer();
		projectExplorer = new ProjectExplorer(projectPath);
		Vector<PackageModel> packages = projectExplorer.getProjectPackages();
		for (PackageModel pckg : packages) {
			pckg.setClasses(pckgExplorer.getPckgClasses(projectPath + "\\bin\\" +  pckg.getName().replace(".", "\\")));
		}
		project = new ProjectModel(getLastStringAfterSequence(projectPath, "\\\\"), packages);
		project.setRelations(getProjectRelations());
	}
	
	public ProjectModel getProject() {
		return project;
	}
	
	public Vector<ClassModel> getProjectClasses() {
		Vector<ClassModel> classes = new Vector<>();
		Vector<PackageModel> packages = project.getPackages();
		for (PackageModel pckg : packages) {
			classes.addAll(pckgExplorer.getPckgClasses(projectPath + "\\bin\\" +  pckg.getName().replace(".", "\\")));
		}
		return classes;
	}
	
	public Set<Relation> getProjectRelations() {
		ProjectRelationsExplorer explorer = new ProjectRelationsExplorer(getProjectClasses());
		Set<Relation> relations = explorer.getClassesRelations();
		return relations;
	}
	
	public void saveProjectAsXmlFile() {
		String xmlContent = project.toXmlContent();
		saveContentAsXml(xmlContent, XML_PROJECTS + project.getName() + ".xml");
	}
	
	public ProjectModel loadProjectFromXmlFile(String source) {
		ProjectSaxParser parser = new ProjectSaxParser(source);
		return parser.getProject();
	}

}