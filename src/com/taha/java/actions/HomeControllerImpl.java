package com.taha.java.actions;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.Method;

import javax.swing.JFrame;
import javax.swing.JScrollPane;

import com.taha.java.annotations.Action;
import com.taha.java.services.ProjectServices;
import com.taha.java.ui.classdiagram.ClassDiagram;
import com.taha.java.ui.components.CustomButton;
import com.taha.java.ui.components.DirectoryChooser;
import com.taha.java.ui.components.FileChooser;
import com.taha.java.ui.components.MessageBox;
import com.taha.java.ui.components.MessageType;
import com.taha.java.ui.components.RadioPanel;
import com.taha.java.ui.pckgdiagram.PackageDiagram;
import com.taha.java.utils.FileHandler;

public class HomeControllerImpl implements ActionListener, HomeController {

	private static boolean isDarkMode = false;
	
	private final String ERROR_JAVA_PROJECT = "<html>"
			+ "Something Went Wrong ! "
			+ "<br/>"
			+ "Bin not found"
			+ "<br/>"
			+ "Please choose a valid Java Project"
			+ "</html>";
	private final String CREATE_CLASS_TEXT = "<html>"
			+ "Create Class Diagram"
			+ "<br/>"
			+ "From Java Project"
			+ "</html>";
	
	private final String CREATE_PACKAGE_TEXT = "<html>"
			+ "Create Package Diagram"
			+ "<br/>"
			+ "From Java Project"
			+ "</html>";
	
	private final String LOAD_CLASS_TEXT = "<html>"
			+ "Load Class Diagram"
			+ "<br/>"
			+ "From XML File"
			+ "</html>";
	
	private final String LOAD_PACKAGE_TEXT = "<html>"
			+ "Load Package Diagram"
			+ "<br/>"
			+ "From XML File"
			+ "</html>";
	
	private final String CREATE_XML_TEXT = "<html>"
			+ "Create XML File"
			+ "<br/>"
			+ "Of Java Project"
			+ "</html>";
	
	private ProjectServices service;	
	
	public void actionPerformed(ActionEvent e) {
		try {
			CustomButton source = (CustomButton) e.getSource();
			RadioPanel panel =(RadioPanel) source.getParent().getParent().getComponent(1);
			isDarkMode = panel.isDarkMode();
			Method method = getClass().getDeclaredMethod(source.getActionName());
			method.invoke(this);
			
		} catch (Exception e1) { }
	}
	
	@Action(value = CREATE_CLASS_TEXT, icon = "java.png")
	public void createClassDiagramFromProject() {
		JFrame frame = new JFrame();
		DirectoryChooser dirChooser = new DirectoryChooser("Select your Java Project");
		dirChooser.showOpenDialog(frame);
		if (dirChooser.getSelectedFile() != null) {
			String source = dirChooser.getSelectedFile().getAbsolutePath();
			if(FileHandler.isJavaProject(source)) {
				service = new ProjectServices(source);
				JScrollPane pane = new JScrollPane(new ClassDiagram(service.getProject(), isDarkMode));
				frame.setContentPane(pane);
				frame.validate();
				frame.pack();
				frame.setLocationRelativeTo(null);
				frame.setVisible(true);
			}
			else {
				MessageBox.infoBox(ERROR_JAVA_PROJECT,
						"UML Diagram Generator",
						MessageType.ERROR);
			}			
		}
	}

	@Action(value = LOAD_CLASS_TEXT, icon = "xml-extension.png")
	public void loadClassDiagram() {
		JFrame frame = new JFrame();
		FileChooser fileChooser = new FileChooser("Select your XML/XMI Project File", ".xml");
		fileChooser.showOpenDialog(frame);
		if (fileChooser.getSelectedFile() != null) {
			String source = fileChooser.getSelectedFile().getAbsolutePath();
			service = new ProjectServices("");
			JScrollPane pane = new JScrollPane(new ClassDiagram(service.loadProjectFromXmlFile(source), isDarkMode));
			frame.setContentPane(pane);
			frame.validate();
			frame.pack();
			frame.setLocationRelativeTo(null);
			frame.setVisible(true);
		}
	}

	@Action(value = CREATE_PACKAGE_TEXT, icon = "java.png")
	public void createPackageDiagramFromProject() {
		JFrame frame = new JFrame();
		DirectoryChooser dirChooser = new DirectoryChooser("Select your Java Project");
		dirChooser.showOpenDialog(frame);
		if (dirChooser.getSelectedFile() != null) {
			String source = dirChooser.getSelectedFile().getAbsolutePath();
			if(FileHandler.isJavaProject(source)) {
				service = new ProjectServices(source);
				JScrollPane pane = new JScrollPane(new PackageDiagram(service.getProject(), isDarkMode));
				frame.setContentPane(pane);
				frame.validate();
				frame.pack();
				frame.setLocationRelativeTo(null);
				frame.setVisible(true);
			}
			else {
				MessageBox.infoBox(ERROR_JAVA_PROJECT,
						"UML Diagram Generator",
						MessageType.ERROR);
			}
		}
	}

	@Action(value = LOAD_PACKAGE_TEXT, icon = "xml-extension.png")
	public void loadPackageDiagram() {
		JFrame frame = new JFrame();
		FileChooser fileChooser = new FileChooser("Select your XML/XMI Project File", ".xml");
		fileChooser.showOpenDialog(frame);
		if (fileChooser.getSelectedFile() != null) {
			String source = fileChooser.getSelectedFile().getAbsolutePath();
			service = new ProjectServices("");
			JScrollPane pane = new JScrollPane(new PackageDiagram(service.loadProjectFromXmlFile(source), isDarkMode));
			frame.setContentPane(pane);
			frame.validate();
			frame.pack();
			frame.setLocationRelativeTo(null);
			frame.setVisible(true);
		}
	}

	@Action(value = CREATE_XML_TEXT, icon = "xml-extension.png")
	public void createXml() {
		JFrame frame = new JFrame();
		DirectoryChooser dirChooser = new DirectoryChooser("Select your Java Project");
		dirChooser.showOpenDialog(frame);
		if (dirChooser.getSelectedFile() != null) {
			String source = dirChooser.getSelectedFile().getAbsolutePath();
			if(FileHandler.isJavaProject(source)) {
				service = new ProjectServices(source);
				service.saveProjectAsXmlFile();
				MessageBox.infoBox("Your Project XML Representation was created successfully",
						"UML Diagram Generator",
						MessageType.INFO);
			}
			else {
				MessageBox.infoBox(ERROR_JAVA_PROJECT,
						"UML Diagram Generator",
						MessageType.ERROR);
			}
		}
	}
	
}
