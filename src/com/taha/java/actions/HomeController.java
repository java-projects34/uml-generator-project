package com.taha.java.actions;

public interface HomeController {

	public void createClassDiagramFromProject();
	public void createPackageDiagramFromProject();
	public void loadClassDiagram();
	public void loadPackageDiagram();
	public void createXml();
	
}
