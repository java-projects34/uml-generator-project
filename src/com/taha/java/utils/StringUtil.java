package com.taha.java.utils;

public class StringUtil {

	
	public static String getLastStringAfterSequence(String s, String sequence) {
		String[] split = s.split(sequence);
		return split[split.length - 1];
	}
}
