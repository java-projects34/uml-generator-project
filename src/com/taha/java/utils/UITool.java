package com.taha.java.utils;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JSeparator;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;


public class UITool {
	
	public static final Color CLASS_COLOR = Color.decode("#ebf4fe"); 
	public static final Color CLASS_COLOR_DARK = Color.decode("#333533"); 
	public static final Color HEADER_COLOR = Color.decode("#2c84b9"); 
	public static final Color HEADER_COLOR_DARK = Color.decode("#F5CB5C"); 
	public static final Color DIAGRAM_COLOR_DARK = Color.decode("#242423"); 
	public static final Color HOME_BG_COLOR = Color.decode("#3F53D9");
	public static final Color TRASNPARENT_COLOR = new Color(0, 0, 0, 0);
	public static final ColorMode DARK_MODE = new ColorMode(HEADER_COLOR_DARK, CLASS_COLOR_DARK, DIAGRAM_COLOR_DARK, Color.white, Color.black);
	public static final ColorMode LIGHT_MODE = new ColorMode(HEADER_COLOR, CLASS_COLOR, Color.white, Color.black, Color.white);
	
	public static Border getBorderWithMargin(int margin, Color color, int thickness) {
		return BorderFactory.createCompoundBorder(new EmptyBorder(margin, margin, margin, margin), new LineBorder(color, thickness));
	}
	
	public static Border getEmptyBorderWithMargin(int margin) {
		return BorderFactory.createEmptyBorder(margin, margin, margin, margin);
	}

	public static JSeparator getSeparator(int orientation, Color color) {
		JSeparator separator = new JSeparator(orientation);
		separator.setForeground(color);
		return separator;
	}
	
	public static Font getEntityTypeFont() {
		return new Font("Trebuchet MS", Font.BOLD, 10);
	}
	
	public static Font getClassNameFont() {
		return new Font("Trebuchet MS", Font.PLAIN, 10);
	}	
	
	public static Dimension getScreenSize() {
		return Toolkit.getDefaultToolkit().getScreenSize();
	}	
	
	public static JFrame getFrame(Component c) {
		if (c.getClass().getSuperclass().equals(JFrame.class)) return (JFrame)c;
		else return getFrame(c.getParent());
	}

	public static ImageIcon getAppIcon() {
		return new ImageIcon("./resources/assets/icons/uml.png");
	}
	
}
