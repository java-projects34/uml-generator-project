package com.taha.java.utils;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class FileHandler {
	
	public final static String XML_DECLARATION = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";

	public static void saveContentAsXml(String content, String filePath) {
		try {
			FileWriter out = new FileWriter(new File(filePath));
			out.write(XML_DECLARATION + content);
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static boolean isJavaProject(String dirPath) {
		File bin = new File(dirPath + "/bin");
		return bin.exists();
	}
	
}