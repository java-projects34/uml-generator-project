package com.taha.java.utils;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.Rectangle;
import java.awt.Stroke;
import java.util.Random;
import java.util.Vector;

import javax.swing.JPanel;

import com.taha.java.models.RelationType;
import com.taha.java.ui.classdiagram.ClassPanel;

public class GraphicsTool {

	private Graphics2D g;

	public GraphicsTool(Graphics g) {
		this.g = (Graphics2D) g;
	}

	public void drawRelation(JPanel source, JPanel target, RelationType type) {

		Point targetPoint = new Point();
		Point sourcePoint = getClosestPoint(targetPoint, getPanelRealBounds(source, ClassPanel.CLASSPANELMARGIN));

		if (type.equals(RelationType.AGGREGATION)) {
			targetPoint = getLeftPoint(target);
			Polygon p = new Polygon();
			p.addPoint(targetPoint.x + 50, targetPoint.y);
			p.addPoint(targetPoint.x + 35, targetPoint.y - 10);
			p.addPoint(targetPoint.x + 20, targetPoint.y);
			p.addPoint(targetPoint.x + 35, targetPoint.y + 10);
			g.drawPolygon(p);
			drawLineWithPoints(g,
					targetPoint,
					new Point(targetPoint.x + 20, targetPoint.y));
		}		
		else if (type.equals(RelationType.COMPOSITION)) {
			targetPoint = getRightPoint(target);
			Polygon p = new Polygon();
			p.addPoint(targetPoint.x - 50, targetPoint.y - 20);
			p.addPoint(targetPoint.x - 35, targetPoint.y - 30);
			p.addPoint(targetPoint.x - 20, targetPoint.y - 20);
			p.addPoint(targetPoint.x - 35, targetPoint.y - 10);
			g.fillPolygon(p);
			drawLineWithPoints(g,
					targetPoint,
					new Point(targetPoint.x - 20, targetPoint.y - 20));
		}		
		else if (type.equals(RelationType.EXTENDS)) {
			targetPoint = getBottomPoint(target);
			Polygon p = new Polygon();
			p.addPoint(targetPoint.x, targetPoint.y - 50);
			p.addPoint(targetPoint.x - 10, targetPoint.y - 30);
			p.addPoint(targetPoint.x + 10, targetPoint.y - 30);
			g.drawPolygon(p);
			drawLineWithPoints(g,
					targetPoint,
					new Point(targetPoint.x, targetPoint.y - 30));
		}		
		else if (type.equals(RelationType.ASSOCIATION)) {
			targetPoint = getTopPoint(target);

			drawLineWithPoints(g,
					targetPoint,
					new Point(targetPoint.x, targetPoint.y + 50));
		}
		else if (type.equals(RelationType.IMPLEMENTATION)) {
			targetPoint = getBottomPoint(target);
			Polygon p = new Polygon();
			p.addPoint(targetPoint.x, targetPoint.y - 50);
			p.addPoint(targetPoint.x - 10, targetPoint.y - 30);
			p.addPoint(targetPoint.x + 10, targetPoint.y - 30);
			g.drawPolygon(p);
			drawLineWithPoints(g,
					targetPoint,
					new Point(targetPoint.x, targetPoint.y - 30));

			Stroke dashed = new BasicStroke(3, BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL,
					0, new float[]{9}, 0);
			g.setStroke(dashed);

			g.drawLine(targetPoint.x, targetPoint.y, sourcePoint.x, sourcePoint.y);
			g.setStroke(new BasicStroke());
		} 
		if (!type.equals(RelationType.IMPLEMENTATION)) {
			drawLineWithPoints(g, targetPoint, sourcePoint);
		}
	}

	public Point getTopPoint(JPanel panel) {
		int x = (int) panel.getLocation().getX() + (panel.getWidth() / 2);
		int y = (int) panel.getLocation().getY() + ClassPanel.CLASSPANELMARGIN;
		return new Point(x, y - 50);
	}

	public Point getBottomPoint(JPanel panel) {
		int x = (int) panel.getLocation().getX() + (panel.getWidth() / 2);
		int y = (int) panel.getLocation().getY() + (panel.getHeight()) - ClassPanel.CLASSPANELMARGIN;
		return new Point(x, y + 50);
	}

	public Point getLeftPoint(JPanel panel) {
		int x = (int) panel.getLocation().getX() + ClassPanel.CLASSPANELMARGIN;
		int y = (int) panel.getLocation().getY() + (panel.getHeight() / 4);
		return new Point(x - 50, y + 50);
	}

	public Point getRightPoint(JPanel panel) {
		int x = (int) panel.getLocation().getX() + panel.getWidth() - ClassPanel.CLASSPANELMARGIN;
		int y = (int) panel.getLocation().getY() + (panel.getHeight() / 4);
		return new Point(x + 50, y + 50);
	}


	public Rectangle getPanelRealBounds(JPanel panel, int margin){
		Rectangle bounds = new Rectangle(
				new Point(panel.getLocation().x + margin,
						panel.getLocation().y + margin),
				new Dimension(panel.getWidth() - (margin * 2),
						panel.getHeight() - (margin * 2)));
		return bounds;
	}

	public Point getClosestPoint(Point point, Rectangle rect) { 
		int x = Math.max(rect.x, Math.min(point.x, rect.x + rect.width));
		int y = Math.max(rect.y, Math.min(point.y, rect.y + rect.height));
		return new Point(x, y);
	}

	public void drawLineWithPoints(Graphics2D g, Point p1, Point p2) {
        g.setStroke(new BasicStroke(2));
		g.drawLine(p1.x, p1.y, p2.x, p2.y);
	}

	public Vector<Color> generateColors(int number) {
		Vector<Color> colors = new Vector<>();
		Random rand = new Random();
		for (int i = 0; i < number; i++) {
			int red = rand.nextInt(150);
			int green = rand.nextInt(150);
			int blue = rand.nextInt(150);
			colors.add(new Color(red, green, blue));
		}
		return colors;
	}
}
