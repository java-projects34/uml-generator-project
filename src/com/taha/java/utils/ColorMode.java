package com.taha.java.utils;

import java.awt.Color;

public class ColorMode {

	private Color headerColor;
	private Color contentColor;
	private Color containerColor;
	private Color membersColor;
	private Color entityColor;
	
	public ColorMode() {
		// TODO Auto-generated constructor stub
	}

	public ColorMode(Color headerColor, Color contentColor, Color containerColor, Color membersColor, Color entityColor) {
		super();
		this.headerColor = headerColor;
		this.contentColor = contentColor;
		this.containerColor = containerColor;
		this.membersColor = membersColor;
		this.entityColor = entityColor;
	}

	public Color getHeaderColor() {
		return headerColor;
	}

	public void setHeaderColor(Color headerColor) {
		this.headerColor = headerColor;
	}

	public Color getContentColor() {
		return contentColor;
	}

	public void setContentColor(Color contentColor) {
		this.contentColor = contentColor;
	}

	public Color getContainerColor() {
		return containerColor;
	}

	public void setContainerColor(Color containerColor) {
		this.containerColor = containerColor;
	}

	public Color getMembersColor() {
		return membersColor;
	}

	public void setMembersColor(Color membersColor) {
		this.membersColor = membersColor;
	}

	public Color getEntityColor() {
		return entityColor;
	}

	public void setEntityColor(Color entityColor) {
		this.entityColor = entityColor;
	}
	
}
