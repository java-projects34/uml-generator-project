package com.taha.java.utils;

import java.io.File;
import java.net.URL;
import java.net.URLClassLoader;

import com.taha.java.ui.components.MessageBox;
import com.taha.java.ui.components.MessageType;

public class ClassHandler {
	
	public ClassHandler() {
		
	}
	
	@SuppressWarnings("resource")
	public static Class<?> loadClass(String projectPath, String className) {
		Class<?> cls = null;
		File file = new File(projectPath + "\\bin\\");
		URL url;
		try {
			url = file.toURI().toURL();
			URL[] urls = new URL[]{url};
			URLClassLoader classLoader = new URLClassLoader(urls);
			cls = classLoader.loadClass(className);
		} catch (Exception e) {
			MessageBox.infoBox("Failed to load Entity : " + className, "Loading Error", MessageType.ERROR);
		}
		return cls;
	}
	
	public static boolean isUserDefined(Class<?> cls) {
		return !("".getClass().getClassLoader() == cls.getClassLoader());		
	}
	
}