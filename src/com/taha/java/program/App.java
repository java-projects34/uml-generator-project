package com.taha.java.program;

import javax.swing.JFrame;
import com.taha.java.ui.Home;

public class App extends JFrame {

	private static final long serialVersionUID = 1L;
	
	public App() {
		config();
		launch();
	}
	
	void launch() {
		setTitle("UML Diagram Generator");
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		pack();
		setLocationRelativeTo(null);
		setVisible(true);
		// test
	}
	
	void config() {
		setContentPane(new Home());
	}

	public static void main(String[] args) {
		new App();
	}
}
