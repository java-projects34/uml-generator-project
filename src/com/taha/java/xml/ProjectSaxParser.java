package com.taha.java.xml;

import java.util.Vector;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.taha.java.models.ClassModel;
import com.taha.java.models.EntityType;
import com.taha.java.models.FieldModel;
import com.taha.java.models.MethodModel;
import com.taha.java.models.PackageModel;
import com.taha.java.models.ProjectModel;
import com.taha.java.models.Relation;
import com.taha.java.models.RelationType;
import com.taha.java.utils.StringUtil;


public class ProjectSaxParser extends DefaultHandler {
	
	private ProjectModel project;
	private boolean param = false;
	private boolean implmentation = false;
	private boolean internalCls = false;
	private Vector<ClassModel> classes;
	private ClassModel entity;
	private ClassModel internalEntity;
	private MethodModel method;
	private Vector<FieldModel> fields;
	private Vector<MethodModel> methods;
	private Vector<String> params;
	private Vector<String> interfaces;
	private Vector<ClassModel> internalClasses;
	
	public ProjectSaxParser(String source) {
		try {
			SAXParserFactory factory = SAXParserFactory.newDefaultInstance();
			SAXParser parser = factory.newSAXParser();
			parser.parse(source, this);
		} catch (Exception e) {
			e.printStackTrace();
		} 
	}
	
	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
		if (qName.equals("project")) {
			project = new ProjectModel();
			project.setName(attributes.getValue("name"));
		}
		if (qName.equals("package")) {
			classes = new Vector<>();
		}
		if (qName.equals("entity") && internalCls == false) {
			entity = new ClassModel();
			String name = attributes.getValue("name");
			String type = attributes.getValue("type");
			String parent = attributes.getValue("extends");
			entity.setName(name);
			entity.setSimpleName(StringUtil.getLastStringAfterSequence(name, "\\."));
			entity.setType(EntityType.valueOf(type.toUpperCase()));
			entity.setParentName(parent);
		}
		if (qName.equals("entity") && internalCls == true){
			internalEntity = new ClassModel();
			String name = attributes.getValue("name");
			String type = attributes.getValue("type");
			String parent = attributes.getValue("extends");
			internalEntity.setName(name);
			internalEntity.setSimpleName(StringUtil.getLastStringAfterSequence(name, "\\."));
			internalEntity.setType(EntityType.valueOf(type.toUpperCase()));
			internalEntity.setParentName(parent);
		}
		if (qName.equals("fields")) {
			fields = new Vector<>();
		}
		if (qName.equals("field")) {
			String name = attributes.getValue("name");
			String type = attributes.getValue("type");
			String visibility = attributes.getValue("visibility");
			fields.add(new FieldModel(name, type, visibility));
		}
		if (qName.equals("constructors") || qName.equals("methods")) {
			methods = new Vector<>();
		}
		if (qName.equals("method")) {
			String name = attributes.getValue("name");
			String returnType = attributes.getValue("return-type");
			String visibility = attributes.getValue("visibility");
			method = new MethodModel(name, returnType, visibility);
		}
		if (qName.equals("parameters")) {
			params = new Vector<>();
		}
		if (qName.equals("param")) {
			param = true;
		}
		
		if (qName.equals("relation")) {
			String type = attributes.getValue("type");
			String startEntity = attributes.getValue("start-entity");
			String endEntity = attributes.getValue("end-entity");
			project.getRelations().add(new Relation(RelationType.valueOf(type.toUpperCase()), startEntity, endEntity));
		}
		if (qName.equals("implementations")) {
			interfaces = new Vector<>();
		}
		if (qName.equals("interface")) {
			implmentation = true;
		}
		if (qName.equals("internal-classes")) {
			internalClasses = new Vector<>();
			internalCls = true;
		}
	}
	
	@Override
	public void endElement(String uri, String localName, String qName) throws SAXException {
		if (qName.equals("package")) {
			String name = entity.getName();
			name = name.replace("." + entity.getSimpleName(), "");
			PackageModel p = new PackageModel(name, classes);
			project.getPackages().add(p);
		}
		if (qName.equals("entity")) {
			if (internalCls == true) {
				internalClasses.add(internalEntity);
			}
			else {
				classes.add(entity);	
			}
		}
		if (qName.equals("fields")) {
			entity.setFields(fields);
		}
		if (qName.equals("methods")) {
			entity.setMethods(methods);
		}
		if (qName.equals("constructors")) {
			entity.setConstructors(methods);
		}
		if (qName.equals("method")) {
			methods.add(method);
		}
		if (qName.equals("parameters")) {
			method.setParametersType(params);
		}
		if (qName.equals("param")) {
			param = false;
		}
		if (qName.equals("implementations")) {
			entity.setInterfaces(interfaces);
		}
		if (qName.equals("interface")) {
			implmentation = false;
		}
		if (qName.equals("internal-classes")) {
			internalCls = false;
		}
	}
	
	@Override
	public void characters(char[] ch, int start, int length) throws SAXException {
		if (param == true) {
			String s = new String(ch, start, length);
			params.add(s);
		}
		if (implmentation == true) {
			String s = new String(ch, start, length);
			interfaces.add(s);
		}
	}
	
	public ProjectModel getProject() {
		return project;
	}
}