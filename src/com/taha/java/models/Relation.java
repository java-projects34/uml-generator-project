package com.taha.java.models;

import java.util.Objects;

public class Relation {
	
	private RelationType type;
	private String startEntity;
	private String endEntity;

	public Relation() {

	}

	public Relation(RelationType type, String startEntity, String endEntity) {
		super();
		this.type = type;
		this.startEntity = startEntity;
		this.endEntity = endEntity;
	}

	public RelationType getType() {
		return type;
	}

	public void setType(RelationType type) {
		this.type = type;
	}

	public String getStartEntity() {
		return startEntity;
	}

	public void setStartEntity(String startEntity) {
		this.startEntity = startEntity;
	}

	public String getEndEntity() {
		return endEntity;
	}

	public void setEndEntity(String endEntity) {
		this.endEntity = endEntity;
	}

	@Override
	public String toString() {
		return "Relation [type=" + type + ", startEntity=" + startEntity + ", endEntity=" + endEntity + "]";
	}

	@Override
	public int hashCode() {
		return Objects.hash(endEntity, startEntity, type);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Relation other = (Relation) obj;
		return Objects.equals(endEntity, other.endEntity) && Objects.equals(startEntity, other.startEntity)
				&& type == other.type;
	}

	public String toXmlContent() {
		String content = "<relation type=\"" + type.getName() + "\" start-entity=\"" + startEntity + "\" end-entity=\"" + endEntity + "\"/>";
		return content;
	}

	
}
