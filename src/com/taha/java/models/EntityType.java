package com.taha.java.models;

public enum EntityType {

	CLASS(0, "class"), INTERFACE(1, "interface"), ENUM(2, "enum"), ANNOTATION(3, "annotation");
	
	private int id;
	private String name;

	private EntityType(int id, String name) {
		this.id = id;
		this.name = name;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}

