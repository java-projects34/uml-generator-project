package com.taha.java.models;

public enum RelationType {

	ASSOCIATION(0, "association"),
	EXTENDS(1, "extends"),
	AGGREGATION(2, "aggregation"),
	COMPOSITION(3, "composition"),
	IMPLEMENTATION(3, "implementation");

	private int id;
	private String name;

	private RelationType(int id, String name) {
		this.id = id;
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}


}

