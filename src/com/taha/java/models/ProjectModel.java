package com.taha.java.models;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.Vector;

public class ProjectModel {

	private String name;
	private Vector<PackageModel> packages;
	private Set<Relation> relations;
	
	public ProjectModel() {
		packages = new Vector<>();
		relations = new HashSet<>();
	}

	public ProjectModel(String name, Vector<PackageModel> packages) {
		super();
		this.name = name;
		this.packages = packages;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Vector<PackageModel> getPackages() {
		return packages;
	}

	public void setPackages(Vector<PackageModel> packages) {
		this.packages = packages;
	}

	public Set<Relation> getRelations() {
		return relations;
	}

	public void setRelations(Set<Relation> relations) {
		this.relations = relations;
	}
	
	public Vector<ClassModel> getAllClasses(){
		Vector<ClassModel> classes = new Vector<>();
		for (PackageModel pckg : packages) {
			classes.addAll(pckg.getClasses());
		}
		return classes;
	}

	public String toXmlContent() {
		String content = "<project name=\"" + name + "\">";
		for (PackageModel pckg : packages) {
			content += pckg.toXmlContent();
		}
		for (Relation relation : relations) {
			content += relation.toXmlContent();
		}
		content += "</project>";
		return content;
	}

	@Override
	public int hashCode() {
		return Objects.hash(name, packages, relations);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProjectModel other = (ProjectModel) obj;
		return Objects.equals(name, other.name) && Objects.equals(packages, other.packages)
				&& Objects.equals(relations, other.relations);
	}
	
	
}
