package com.taha.java.models;

import java.util.Objects;
import java.util.Vector;
import static com.taha.java.utils.StringUtil.getLastStringAfterSequence;

public class MethodModel implements Member {
	
	private String name;
	private String returnType;
	private String visibility;
	private Vector<String> parametersType;

	public MethodModel() {
		
	}

	public MethodModel(String name, String returnType, String visibility, Vector<String> parametersType) {
		super();
		this.name = name;
		this.returnType = returnType;
		this.visibility = visibility;
		this.parametersType = parametersType;
	}

	public MethodModel(String name, String returnType, String visibility) {
		super();
		this.name = name;
		this.returnType = returnType;
		this.visibility = visibility;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getReturnType() {
		return returnType;
	}

	public void setReturnType(String returnType) {
		this.returnType = returnType;
	}

	public String getVisibility() {
		return visibility;
	}

	public void setVisibility(String visibility) {
		this.visibility = visibility;
	}

	public Vector<String> getParametersType() {
		return parametersType;
	}

	public void setParametersType(Vector<String> parametersType) {
		this.parametersType = parametersType;
	}

	@Override
	public String toString() {
		String s = visibility + " " + returnType + " " + name + " (";
		if (parametersType.size() > 0) {
			for (String parameterType : parametersType) {
				s += parameterType + ", ";
			}
			s = s.substring(0, s.length() - 2);
		}
		s += ")";
		return s;
	}

	@Override
	public String toUmlContent() {
		String content = "";
		if(visibility.contains("public")) content += "+";
		if(visibility.contains("private")) content += "-";
		if(visibility.contains("protected")) content += "#";
		content += " " + name + " (";
		if (parametersType.size() > 0) {
			for (String param : parametersType) {
				content += getLastStringAfterSequence(param, "\\.") + ", ";
			}
			content = content.substring(0, content.length() - 2);
		}
		content += ") : " + getLastStringAfterSequence(returnType, "\\.");
		return content;
	}

	public String toXmlContent() {
		String content = "<method name=\"" + name + "\" return-type=\"" + returnType + "\" visibility=\"" + visibility + "\">";
		content += "<parameters>";
		for (String param : parametersType) {
			content += "<param>" + param + "</param>";
		}
		content += "</parameters>";
		content += "</method>";
		return content;
	}

	@Override
	public int hashCode() {
		return Objects.hash(name, parametersType, returnType, visibility);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MethodModel other = (MethodModel) obj;
		return Objects.equals(name, other.name) && Objects.equals(parametersType, other.parametersType)
				&& Objects.equals(returnType, other.returnType) && Objects.equals(visibility, other.visibility);
	}
	
	
	
}
