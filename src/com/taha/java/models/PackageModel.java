package com.taha.java.models;

import java.util.Objects;
import java.util.Vector;

public class PackageModel {
	
	private String name;
	private Vector<ClassModel> classes;

	public PackageModel() {
		
	}

	public PackageModel(String name, Vector<ClassModel> classes) {
		super();
		this.name = name;
		this.classes = classes;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Vector<ClassModel> getClasses() {
		return classes;
	}

	public void setClasses(Vector<ClassModel> classes) {
		this.classes = classes;
	}

	public String toXmlContent() {
		String content = "<package name=\"" + name + "\">";
		for (ClassModel cls : classes) {
			content += cls.toXmlContent();
		}
		content += "</package>";
		return content;
	}

	@Override
	public int hashCode() {
		return Objects.hash(classes, name);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PackageModel other = (PackageModel) obj;
		return Objects.equals(classes, other.classes) && Objects.equals(name, other.name);
	}
	
	
}
