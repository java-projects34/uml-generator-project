package com.taha.java.models;

import static com.taha.java.utils.StringUtil.getLastStringAfterSequence;

import java.util.Objects;

public class FieldModel implements Member {
	
	private String name;
	private String type;
	private String visibility;
	
	public FieldModel() {

	}

	public FieldModel(String name, String type, String visibility) {
		super();
		this.name = name;
		this.type = type;
		this.visibility = visibility;
	}

	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getType() {
		return type;
	}


	public void setType(String type) {
		this.type = type;
	}


	public String getVisibility() {
		return visibility;
	}


	public void setVisibility(String visibility) {
		this.visibility = visibility;
	}
	
	@Override
	public String toString() {
		return visibility + " " + type + " " + name;
	}

	@Override
	public String toUmlContent() {
		String content = "";
		if(visibility.contains("public")) content += "+";
		if(visibility.contains("private")) content += "-";
		if(visibility.contains("protected")) content += "#";
		content += " " + name + " : " + getLastStringAfterSequence(type, "\\.");
		return content;
	}

	public String toXmlContent() {
		String content = "<field name=\"" + name + "\" type=\"" + type + "\" visibility=\"" + visibility + "\"/>";
		return content;
	}

	@Override
	public int hashCode() {
		return Objects.hash(name, type, visibility);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FieldModel other = (FieldModel) obj;
		return Objects.equals(name, other.name) && Objects.equals(type, other.type)
				&& Objects.equals(visibility, other.visibility);
	}
	
	
}
