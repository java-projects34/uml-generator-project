package com.taha.java.models;

import java.util.Objects;
import java.util.Vector;

public class ClassModel {
	
	private String name;
	private String simpleName;
	private String visibility;
	private EntityType type; 
	private Vector<FieldModel> fields;
	private Vector<MethodModel> methods;
	private Vector<MethodModel> constructors;
	private Vector<String> interfaces;
	private String parentName;
	private Vector<ClassModel> internalClasses;
	
	public ClassModel() {
		fields = new Vector<>();
		methods = new Vector<>();
		constructors = new Vector<>();
		interfaces = new Vector<>();
		internalClasses = new Vector<>();
	}	

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getSimpleName() {
		return simpleName;
	}

	public void setSimpleName(String simpleName) {
		this.simpleName = simpleName;
	}

	
	public String getVisibility() {
		return visibility;
	}

	public void setVisibility(String visibility) {
		this.visibility = visibility;
	}

	public EntityType getType() {
		return type;
	}

	public void setType(EntityType type) {
		this.type = type;
	}

	public Vector<FieldModel> getFields() {
		return fields;
	}

	public void setFields(Vector<FieldModel> fields) {
		this.fields = fields;
	}

	public Vector<MethodModel> getMethods() {
		return methods;
	}

	public void setMethods(Vector<MethodModel> methods) {
		this.methods = methods;
	}

	public Vector<MethodModel> getConstructors() {
		return constructors;
	}

	public void setConstructors(Vector<MethodModel> constructors) {
		this.constructors = constructors;
	}

	public Vector<String> getInterfaces() {
		return interfaces;
	}

	public void setInterfaces(Vector<String> interfaces) {
		this.interfaces = interfaces;
	}

	public String getParentName() {
		return parentName;
	}

	public void setParentName(String parentName) {
		this.parentName = parentName;
	}

	public Vector<ClassModel> getInternalClasses() {
		return internalClasses;
	}

	public void setInternalClasses(Vector<ClassModel> internalClasses) {
		this.internalClasses = internalClasses;
	}

	@Override
	public String toString() {
		String s = visibility + " " + type.getName() + " " + name;
		if (!"".equals(parentName)) {
			s += " extends " + parentName;
		}
		if (interfaces.size() > 0) {
			s += " implements";
			for (String interf : interfaces) {
				s += " " + interf + ",";
			}
			s = s.substring(0, s.length() - 1);
		}
		s += " {\n";
		if (fields.size() > 0) {
			for (FieldModel field : fields) {
				s += "	" +	field.toString() + ";\n";
			}
		}
		if (constructors.size() > 0) {
			for (MethodModel constructor : constructors) {
				s += "	" +	constructor.toString() + ";\n";
			}
		}
		if (methods.size() > 0) {
			for (MethodModel method : methods) {
				s += "	" +	method.toString() + ";\n";
			}
		}
		if (internalClasses.size() > 0) {
			for (ClassModel internalClass : internalClasses) {
				s += "	" +	internalClass.toString() + "\n";
			}
		}
		s += "}";
		return s;
	}
	
	public String toXmlContent() {
		String content = "<entity name=\"" + name +"\" type=\"" + type.getName() + "\" extends=\"" + parentName + "\">";
		content += "<fields>";
		for (FieldModel f : fields) {
			content += f.toXmlContent();
		}
		content += "</fields>";
		content += "<constructors>";
		for (MethodModel c : constructors) {
			content += c.toXmlContent();
		}
		content += "</constructors>";
		content += "<methods>";
		for (MethodModel m : methods) {
			content += m.toXmlContent();
		}
		content += "</methods>";
		content += "<implementations>";
		for (String i : interfaces) {
			content += "<interface>" + i + "</interface>"; 
		}
		content += "</implementations>";
		content += "<internal-classes>";
		for (ClassModel i : internalClasses) {
			content += i.toXmlContent(); 
		}
		content += "</internal-classes>";
		content += "</entity>";
		return content;
	}

	@Override
	public int hashCode() {
		return Objects.hash(constructors, fields, interfaces, internalClasses, methods, name, parentName, simpleName,
				type, visibility);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ClassModel other = (ClassModel) obj;
		return Objects.equals(constructors, other.constructors) && Objects.equals(fields, other.fields)
				&& Objects.equals(interfaces, other.interfaces)
				&& Objects.equals(internalClasses, other.internalClasses) && Objects.equals(methods, other.methods)
				&& Objects.equals(name, other.name) && Objects.equals(parentName, other.parentName)
				&& Objects.equals(simpleName, other.simpleName) && type == other.type
				&& Objects.equals(visibility, other.visibility);
	}
	
	
	
}
