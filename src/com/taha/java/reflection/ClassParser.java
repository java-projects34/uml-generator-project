package com.taha.java.reflection;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Parameter;
import java.util.Vector;

import com.taha.java.models.ClassModel;
import com.taha.java.models.EntityType;
import com.taha.java.models.FieldModel;
import com.taha.java.models.MethodModel;
import com.taha.java.utils.ClassHandler;

public class ClassParser {

	private ClassModel parsedClass;
	
	public ClassParser(Class<?> cls) {
		parseClass(cls);
	}
	
	public ClassModel getParsedClass() {
		return parsedClass;
	}
	
	private void parseClass(Class<?> cls) {
		parsedClass = new ClassModel();
		parsedClass.setName(cls.getName());
		parsedClass.setSimpleName(cls.getSimpleName());
		parsedClass.setVisibility(Modifier.toString(cls.getModifiers()));
		parsedClass.setType(getClassType(cls));
		parsedClass.setFields(getClassFields(cls));
		parsedClass.setMethods(getClassMethods(cls));
		parsedClass.setConstructors(getClassConstructors(cls));
		parsedClass.setInterfaces(getClassInterfaces(cls));
		parsedClass.setInternalClasses(getClassInternalClasses(cls));
		parsedClass.setParentName(getClassParent(cls));
	}

	private String getClassParent(Class<?> cls) {
		if (cls.getSuperclass() != null && ClassHandler.isUserDefined(cls.getSuperclass())) {
			return cls.getSuperclass().getName();
		}
		return "";
	}

	private EntityType getClassType(Class<?> c) {
		if (c.isAnnotation()) {
			return EntityType.ANNOTATION;
		}
		if (c.isInterface()) {
			return EntityType.INTERFACE;
		}
		if (c.isEnum()) {
			return EntityType.ENUM;
		}
		return EntityType.CLASS;
	}
	
	private Vector<FieldModel> getClassFields(Class<?> cls) {
		Vector<FieldModel> f = new Vector<>();
		Field[] fields = cls.getDeclaredFields();
		for (Field field : fields) {
			f.add(new FieldModel(field.getName(),
					field.getType().getTypeName(),
					Modifier.toString(field.getModifiers())));
		}
		return f;
	}
	
	private Vector<MethodModel> getClassMethods(Class<?> cls) {
		Vector<MethodModel> m = new Vector<>();
		Method[] methods = cls.getDeclaredMethods();
		for (Method method : methods) {
			m.add(new MethodModel(method.getName(),
					method.getReturnType().getTypeName(),
					Modifier.toString(method.getModifiers()),
					getMethodParameters(method)));
		}
		return m;
	}
	
	private Vector<MethodModel> getClassConstructors(Class<?> cls) {
		Vector<MethodModel> m = new Vector<>();
		Constructor<?>[] constructors = cls.getDeclaredConstructors();
		for (Constructor<?> constructor : constructors) {
			m.add(new MethodModel(constructor.getName(),
					cls.getName(),
					Modifier.toString(constructor.getModifiers()),
					getConstructorParameters(constructor)));
		}
		return m;
	}
	
	private Vector<String> getMethodParameters(Method method) {
		Parameter[] parameters = method.getParameters();
		Vector<String> p = new Vector<>();
		for (Parameter parameter : parameters) {
			p.add(parameter.getType().getTypeName());
		}
		return p;
	}
	
	private Vector<String> getConstructorParameters(Constructor<?> constructor) {
		Parameter[] parameters = constructor.getParameters();
		Vector<String> p = new Vector<>();
		for (Parameter parameter : parameters) {
			p.add(parameter.getType().getTypeName());
		}
		return p;
	}
	
	private Vector<String> getClassInterfaces(Class<?> cls) {
		Class<?>[] interfaces = cls.getInterfaces();
		Vector<String> i = new Vector<>();
		for (Class<?> interfaceObj : interfaces) {
			if (ClassHandler.isUserDefined(interfaceObj)) {
				i.add(interfaceObj.getName());
			}
		}
		return i;
	}
	
	private Vector<ClassModel> getClassInternalClasses(Class<?> cls) {
		Class<?>[] internalClasses = cls.getDeclaredClasses();
		Vector<ClassModel> i = new Vector<>();
		for (Class<?> internalClass : internalClasses) {
			ClassParser parser = new ClassParser(internalClass);
			i.add(parser.getParsedClass());
		}
		return i;
	}
}
