package com.taha.java.reflection;

import java.util.HashSet;
import java.util.Set;
import java.util.Vector;

import com.taha.java.models.ClassModel;
import com.taha.java.models.FieldModel;
import com.taha.java.models.MethodModel;
import com.taha.java.models.Relation;
import com.taha.java.models.RelationType;

public class ProjectRelationsExplorer {

	private Vector<String> classesNames;
	private Vector<ClassModel> projectClasses;
	
	public ProjectRelationsExplorer(Vector<ClassModel> projectClasses) {
		this.projectClasses = projectClasses;
		this.classesNames = new Vector<>();
		for (ClassModel cls : projectClasses) {
			classesNames.add(cls.getName());			
		}
	}
	
	public Set<Relation> getClassesRelations(){
		Set<Relation> relations = new HashSet<Relation>();
		relations.addAll(getAggregationRelations());
		relations.addAll(getAssociationRelations());
		relations.addAll(getInheritanceRelations());
		relations.addAll(getCompositionRelations());
		relations.addAll(getImplmRelations());
		return relations;
	}
	
	private Set<Relation> getAggregationRelations() {
		Set<Relation> r = new HashSet<Relation>();
		
		for (ClassModel cls : projectClasses) {
			Vector<FieldModel> fields = cls.getFields();
			for (FieldModel field : fields) {
				if (classesNames.contains(field.getType()) && !cls.getName().equals(field.getType())) {
					r.add(new Relation(RelationType.AGGREGATION, cls.getName(), field.getType()));
				}
			}
		}
		return r;
	}
	
	private Set<Relation> getInheritanceRelations() {
		Set<Relation> r = new HashSet<Relation>();
		for (ClassModel cls : projectClasses) {
			r.add(new Relation(RelationType.EXTENDS, cls.getName(), cls.getParentName()));
		}
		return r;
	}
	
	private Set<Relation> getAssociationRelations() {
		Set<Relation> r = new HashSet<Relation>();
		for (ClassModel cls : projectClasses) {
			Vector<MethodModel> methods = cls.getMethods();
			methods.addAll(cls.getConstructors());
			for (MethodModel method : methods) {
				Vector<String> params = method.getParametersType();
				for (String param : params) {
					if (classesNames.contains(param) && !cls.getName().equals(param)) {
						r.add(new Relation(RelationType.ASSOCIATION, cls.getName(), param));
					}
				}
			}
		}
		return r;
	}
	
	private Set<Relation> getCompositionRelations() {
		Set<Relation> r = new HashSet<Relation>();
		for (ClassModel cls : projectClasses) {
			Vector<ClassModel> internalClasses = cls.getInternalClasses();
			for (ClassModel internalClass : internalClasses) {
				r.add(new Relation(RelationType.COMPOSITION, cls.getName(), internalClass.getName()));
			}
		}
		return r;
	}
	
	private Set<Relation> getImplmRelations() {
		Set<Relation> r = new HashSet<Relation>();
		for (ClassModel cls : projectClasses) {
			Vector<String> interfaces = cls.getInterfaces();
			for (String i : interfaces) {
				r.add(new Relation(RelationType.IMPLEMENTATION, cls.getName(), i));
			}
		}
		return r;
	}
}