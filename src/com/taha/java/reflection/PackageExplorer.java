package com.taha.java.reflection;

import java.io.File;
import java.util.Vector;

import com.taha.java.models.ClassModel;
import com.taha.java.utils.ClassHandler;


public class PackageExplorer {

	public PackageExplorer() {
		
	}

	public Vector<ClassModel> getPckgClasses(String packagePath) {
		String[] split = packagePath.split("\\\\");
		String packageName = "";
		int index = split.length - 1;
		String projectPath = "";
		while(!split[index].equals("bin")) { packageName = split[index--] + "." + packageName; }
		for (int i = 0; i < index; i++) {
			projectPath += split[i] + "\\";
		}
		packageName = packageName.substring(0, packageName.length() - 1);
		projectPath = projectPath.substring(0, projectPath.length() - 1);

		File dir = new File(packagePath);
		File f[] = dir.listFiles();
		Vector<ClassModel> classes = new Vector<>();
		for (int i = 0; i < f.length; i++) {
			if(f[i].isFile() && f[i].getName().endsWith(".class")) {
				String name = f[i].getName().replace(".class", "");
				try {
					ClassParser parser = new ClassParser(ClassHandler.loadClass(projectPath, packageName + "." + name));
					classes.add(parser.getParsedClass());
				} catch (Exception e) {
					System.out.println("Error parsing class : " + name + " !!!");
				}
			}
		}
		return classes;
	}
	
	
}
