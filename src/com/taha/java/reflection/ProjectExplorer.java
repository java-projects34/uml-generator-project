package com.taha.java.reflection;

import java.io.File;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.Vector;

import com.taha.java.models.PackageModel;

public class ProjectExplorer {

	private final String projectBinDir;
	
	public ProjectExplorer(String projectPath) { 
		projectBinDir = projectPath + "/bin";		
	}

	public Vector<PackageModel> getProjectPackages() {
		Set<String> packages = new HashSet<>();
		File binDirectory = new File(projectBinDir);
		findPackages(binDirectory, "", packages);
		
		Vector<PackageModel> p = new Vector<>();
		for (String name : packages) {
			PackageModel pckg = new PackageModel();
			pckg.setName(name);
			p.add(pckg);
		}
		return p;
	}
	
	private void findPackages(File directory, String packageName, Set<String> packages) {
		
		if (!directory.exists()) { return; }
		File[] files = directory.listFiles();

		for (File file : files) {
			String fileName = file.getName();
			if (file.isDirectory()) {
				findPackages(file, packageName + fileName + ".", packages);
			} 
			else if (fileName.endsWith(".class")) {
				try {
					String[] split = file.getAbsolutePath().split("\\\\");
					int index = Arrays.asList(split).indexOf("bin");
					String name = "";
					for (int i = index + 1; i < split.length - 1; i++) {
						name += split[i] + ".";
					}
					name = name.substring(0, name.length() - 1);
					packages.add(name);
				} catch (Exception e) { }
			}
		}
	}
	
	
}
