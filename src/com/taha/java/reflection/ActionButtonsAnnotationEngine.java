package com.taha.java.reflection;

import java.awt.GridLayout;
import java.lang.reflect.Method;

import javax.swing.JPanel;

import com.taha.java.actions.HomeController;
import com.taha.java.annotations.Action;
import com.taha.java.ui.components.CustomButton;
import com.taha.java.utils.UITool;

public class ActionButtonsAnnotationEngine {

	private JPanel buttonsContainer;
	private final String ICONS_FOLDER = "./resources/assets/icons/";
	
	public ActionButtonsAnnotationEngine(HomeController action) {
		buttonsContainer = new JPanel();
		buttonsContainer.setBackground(UITool.CLASS_COLOR_DARK);
		Method[] methods = action.getClass().getDeclaredMethods();
		buttonsContainer.setLayout(new GridLayout(methods.length / 2, 2, 10, 10));
		for (Method method : methods) {
			Action a = method.getDeclaredAnnotation(Action.class);
				if(a != null) {
					buttonsContainer.add(new CustomButton(a.value(),
							ICONS_FOLDER + a.icon(),
							method.getName()));
				}
		}		
	}
	
	public JPanel getButtons() {
		return buttonsContainer;
	}
}
