package com.taha.java.ui;

import java.awt.Color;
import java.awt.FlowLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;

import com.taha.java.ui.components.Label;
import com.taha.java.utils.UITool;

public class HomeHeader extends JPanel {

	private static final long serialVersionUID = 1L;

	public HomeHeader() {
		setLayout(new FlowLayout());
		setBackground(UITool.CLASS_COLOR_DARK);
		JLabel icon = new JLabel(UITool.getAppIcon());
		icon.setAlignmentX(CENTER_ALIGNMENT);
		Label name = new Label("Welcome to UML Diagram Generator",UITool.getClassNameFont(), Color.white, 20f, 5);
		name.setAlignmentX(CENTER_ALIGNMENT);
		add(icon);
		add(name);
	}
}
