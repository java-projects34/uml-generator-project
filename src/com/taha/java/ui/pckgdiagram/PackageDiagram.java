package com.taha.java.ui.pckgdiagram;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.util.Vector;

import javax.swing.JPanel;

import com.taha.java.models.PackageModel;
import com.taha.java.models.ProjectModel;
import com.taha.java.utils.UITool;

public class PackageDiagram extends JPanel {

	private static final long serialVersionUID = 1L;

	public PackageDiagram(ProjectModel project, boolean isDarkMode) {
		setLayout(getLayout());
		setLayout(new FlowLayout());
		Vector<PackageModel> packages = project.getPackages();
		for (PackageModel p : packages) {	
			add(new PackagePanel(p, isDarkMode ? UITool.DARK_MODE : UITool.LIGHT_MODE));
		}
		setPreferredSize(new Dimension(UITool.getScreenSize().width, getTempHeight()));
		setBackground(isDarkMode ? UITool.DIAGRAM_COLOR_DARK : Color.white);
	}

	private int getTempHeight() {
		int height = 0;
		Component[] components = getComponents();
		for (Component c : components) {
			height += c.getPreferredSize().height;
		}
		return height;
	}
}
