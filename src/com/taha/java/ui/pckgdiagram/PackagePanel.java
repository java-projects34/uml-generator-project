package com.taha.java.ui.pckgdiagram;

import static com.taha.java.utils.UITool.getBorderWithMargin;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Polygon;

import javax.swing.JPanel;

import com.taha.java.models.PackageModel;
import com.taha.java.utils.ColorMode;

public class PackagePanel extends JPanel {

	private static final long serialVersionUID = 1L;

	private static final int PACKAGE_PANEL_MARGIN = 50;
	private ColorMode mode;
	
	public PackagePanel(PackageModel pckg, ColorMode mode) {
		this.mode = mode;
		setName(pckg.getName());
		add(new PackageHeader(pckg.getName(), mode));
		setBorder(getBorderWithMargin(PACKAGE_PANEL_MARGIN, mode.getHeaderColor(), 2));
		setPreferredSize(new Dimension(getPreferredSize().width, 200));
		setBackground(mode.getContainerColor());
	}
	
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		paintHeaderStyle(g);
	}

	private void paintHeaderStyle(Graphics g) {
		g.setColor(mode.getHeaderColor());
		Polygon polygon = new Polygon();
		polygon.addPoint(PACKAGE_PANEL_MARGIN, PACKAGE_PANEL_MARGIN);
		polygon.addPoint(getWidth() - PACKAGE_PANEL_MARGIN - (getWidth() / 4), PACKAGE_PANEL_MARGIN);
		polygon.addPoint(getWidth() - PACKAGE_PANEL_MARGIN - (getWidth() / 3), PACKAGE_PANEL_MARGIN - 20);
		polygon.addPoint(PACKAGE_PANEL_MARGIN, PACKAGE_PANEL_MARGIN - 20);
		g.fillPolygon(polygon);
	}
	
}
