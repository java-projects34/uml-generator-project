package com.taha.java.ui.pckgdiagram;

import static com.taha.java.utils.UITool.getClassNameFont;

import java.awt.FlowLayout;

import javax.swing.JPanel;

import com.taha.java.ui.components.Label;
import com.taha.java.utils.ColorMode;

public class PackageHeader extends JPanel {

	private static final long serialVersionUID = 1L;

	public PackageHeader(String name, ColorMode mode) {
		setBackground(mode.getHeaderColor());
		setLayout(new FlowLayout());
		add(new Label(name,
				getClassNameFont(),
				mode.getEntityColor(),
				17f,
				5));
		
	}
}
