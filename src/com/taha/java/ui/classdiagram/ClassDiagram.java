package com.taha.java.ui.classdiagram;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.util.Set;
import java.util.Vector;

import javax.swing.JPanel;

import com.taha.java.models.ClassModel;
import com.taha.java.models.ProjectModel;
import com.taha.java.models.Relation;
import com.taha.java.utils.GraphicsTool;
import com.taha.java.utils.UITool;

public class ClassDiagram extends JPanel {

	private static final long serialVersionUID = 1L;

	private Set<Relation> relations;
	private GraphicsTool graphicsTool;
	
	public ClassDiagram(ProjectModel project, boolean isDarkMode) {
		this.relations = project.getRelations();
		setLayout(new FlowLayout());
		Vector<ClassModel> classes = project.getAllClasses();
		for (ClassModel classModel : classes) {	
			add(new ClassPanel(classModel, isDarkMode ? UITool.DARK_MODE : UITool.LIGHT_MODE));
		}
		setPreferredSize(new Dimension(UITool.getScreenSize().width, getTempHeight()));
		setBackground(isDarkMode ? UITool.DIAGRAM_COLOR_DARK : Color.white);
	}	
	
	private void paintRelation(Graphics g) {
		graphicsTool = new GraphicsTool(g);
		int colorIndex = 0;
		Vector<Color> colors = graphicsTool.generateColors(relations.size());
		for(Relation relation : relations) {
			g.setColor(colors.get(colorIndex++));
			JPanel source = getClassPanelByName(relation.getStartEntity());
			JPanel target = getClassPanelByName(relation.getEndEntity());
			if (source != null && target != null) {
				graphicsTool.drawRelation(source, target, relation.getType());				
			}
		}	
	}
	
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		paintRelation(g);
	}

	private ClassPanel getClassPanelByName(String name){
		Component[]  classPanels = getComponents();
		
		for (Component classPanel : classPanels) {
			if (classPanel.getName().equals(name)) {
				return (ClassPanel) classPanel;
			}
		}
		return null;
	}
		
	private int getTempHeight() {
		int height = 0;
		Component[] components = getComponents();
		System.out.println("found " + components.length);
		for (Component c : components) {
			height += c.getPreferredSize().height;
		}
		System.out.println("h : " + height);
		return height;
	}
}
