package com.taha.java.ui.classdiagram;

import javax.swing.BoxLayout;
import javax.swing.JPanel;

import static com.taha.java.utils.UITool.getEntityTypeFont;
import static com.taha.java.utils.UITool.getClassNameFont;

import com.taha.java.models.EntityType;
import com.taha.java.ui.components.Label;
import com.taha.java.utils.ColorMode;

public class ClassHeader extends JPanel {

	private static final long serialVersionUID = 1L;

	public ClassHeader(String name, EntityType type, ColorMode mode) {
		setBackground(mode.getHeaderColor());
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		if(type != EntityType.CLASS) {
			add(new Label("<< " + type.getName() + " >>",
					getEntityTypeFont(),
					mode.getEntityColor(),
					13f,
					5));
		}
		add(new Label(name,
				getClassNameFont(),
				mode.getEntityColor(),
				17f,
				5));
	}

}
