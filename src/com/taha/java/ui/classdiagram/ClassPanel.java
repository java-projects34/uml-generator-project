package com.taha.java.ui.classdiagram;

import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.JPanel;

import static com.taha.java.utils.UITool.getBorderWithMargin;

import com.taha.java.models.ClassModel;
import com.taha.java.utils.ColorMode;

public class ClassPanel extends JPanel {

	private static final long serialVersionUID = 1L;

	public static final int CLASSPANELMARGIN = 50;
	
	public ClassPanel(ClassModel c, ColorMode mode) {
		setName(c.getName());
		setLayout(new BorderLayout());
		setBackground(new Color(0,0,0,0));
		add(new ClassHeader(c.getSimpleName(), c.getType(), mode),  BorderLayout.NORTH);
		add(new MembersPanel(c.getFields(), mode), BorderLayout.CENTER);
		add(new MembersPanel(c.getMethods(), mode), BorderLayout.SOUTH);
		setBorder(getBorderWithMargin(CLASSPANELMARGIN, mode.getHeaderColor(), 2));
	}
	
}
