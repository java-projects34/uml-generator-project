package com.taha.java.ui.classdiagram;

import java.awt.Color;
import java.util.Vector;

import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import static com.taha.java.utils.UITool.getSeparator;

import com.taha.java.models.Member;
import com.taha.java.ui.components.Label;
import com.taha.java.utils.ColorMode;
import com.taha.java.utils.UITool;

public class MembersPanel extends JPanel {

	private static final long serialVersionUID = 1L;
	
	public MembersPanel(Vector<? extends Member> members, ColorMode mode) {
		add(getSeparator(SwingConstants.HORIZONTAL, Color.white));
		setBackground(mode.getContentColor());
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		for (Member member : members) {
			add(new Label(member.toUmlContent(), 
					UITool.getClassNameFont(),
					mode.getMembersColor(),
					13f,
					5));
		}
	}
}