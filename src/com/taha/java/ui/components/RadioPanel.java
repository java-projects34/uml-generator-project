package com.taha.java.ui.components;

import java.awt.Color;

import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.taha.java.utils.UITool;

public class RadioPanel extends JPanel {

	private static final long serialVersionUID = 1L;

	private boolean isDarkMode;
	private JLabel mode;
	private JPanel container;
	private ButtonGroup colorMode;
	
	public RadioPanel() {
		setBackground(UITool.CLASS_COLOR_DARK);
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		container = new JPanel();
		container.setAlignmentX(CENTER_ALIGNMENT);
		container.setBackground(UITool.CLASS_COLOR_DARK);
		colorMode = new ButtonGroup();
		addRadio("Light Mode", UITool.HEADER_COLOR);
		addRadio("Dark Mode", UITool.HEADER_COLOR_DARK);
		mode = new JLabel();
		mode.setAlignmentX(CENTER_ALIGNMENT);
		add(container);
		add(mode);
		setDarkMode(false);
	}
	
	public void addRadio(String value, Color color) {
		Radio btn = new Radio(value, color);
		container.add(btn);
		colorMode.add(btn);
	}

	public boolean isDarkMode() {
		return isDarkMode;
	}

	public void setDarkMode(boolean isDarkMode) {
		if (isDarkMode) {
			mode.setIcon(new ImageIcon("./resources/assets/icons/moon.png"));
		}
		else {
			mode.setIcon(new ImageIcon("./resources/assets/icons/brightness.png"));
		}
		this.isDarkMode = isDarkMode;
	}
	
	
}
