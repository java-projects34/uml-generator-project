package com.taha.java.ui.components;

import javax.swing.JOptionPane;

public class MessageBox {
	
	public static void infoBox(String msg, String title, MessageType type)
    {
        JOptionPane.showMessageDialog(null, msg, title, type.getId());
    }
}
