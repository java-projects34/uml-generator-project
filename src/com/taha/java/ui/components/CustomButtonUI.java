package com.taha.java.ui.components;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;

import javax.swing.AbstractButton;
import javax.swing.JComponent;
import javax.swing.border.EmptyBorder;
import javax.swing.plaf.basic.BasicButtonUI;

import com.taha.java.utils.UITool;

public class CustomButtonUI extends BasicButtonUI {
	
    @Override
    public void installUI(JComponent c) {
        super.installUI(c);
        AbstractButton button = (AbstractButton) c;
        button.setBackground(UITool.HOME_BG_COLOR);
        button.setForeground(Color.white);
        button.setOpaque(false);
        button.setBorder(new EmptyBorder(5, 15, 5, 15));
        Font font = UITool.getEntityTypeFont();
        button.setFont(font.deriveFont(18f));
    }
    
    @Override
    public void paint (Graphics g, JComponent c) {
    	AbstractButton b = (AbstractButton) c;
        paintElevatedBorder(g, b, b.getModel().isPressed() ? 2 : 0);
        super.paint(g, c);
    }
    
    private void paintElevatedBorder (Graphics g, JComponent c, int yOffset) {
        Dimension size = c.getSize();
        Graphics2D g2 = (Graphics2D) g;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g.setColor(c.getBackground().darker());
        g.fillRoundRect(0, yOffset, size.width, size.height - yOffset, 10, 10);
        g.setColor(c.getBackground());
        g.fillRoundRect(0, yOffset, size.width, size.height + yOffset - 5, 10, 10);
    }
}

