package com.taha.java.ui.components;

import java.awt.Color;
import java.awt.Font;

import javax.swing.JLabel;

import com.taha.java.utils.UITool;

public class Label extends JLabel {

	private static final long serialVersionUID = 1L;

	public Label(String value, Font font, Color color, float size, int margin) {
		super(value);
		setFont(font.deriveFont(size));
		setForeground(color);
		setAlignmentX(JLabel.CENTER_ALIGNMENT);
		setBorder(UITool.getEmptyBorderWithMargin(margin));
	}

}
