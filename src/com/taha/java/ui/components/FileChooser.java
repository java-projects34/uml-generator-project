package com.taha.java.ui.components;

import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.UIManager;
import javax.swing.filechooser.FileFilter;

public class FileChooser extends JFileChooser {

	private static final long serialVersionUID = 1L;

	private String extension;
	
	public FileChooser(String title, String extension) {
		super();
		this.extension =extension;
		setDialogTitle(title);
		setFileFilter(new FileChooserFilter());
		setCurrentDirectory(new File("./resources/projects/xml"));
        try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) { }
	}

	private class FileChooserFilter extends FileFilter{

		@Override
		public boolean accept(File f) {
			if (f.getName().endsWith(extension) || f.isDirectory()) {
				return true;
			}
			else {
				return false;
			} 
		}

		@Override
		public String getDescription() {
			return "Files " + extension;
		}
	}


}
