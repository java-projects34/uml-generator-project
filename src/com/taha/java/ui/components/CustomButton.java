package com.taha.java.ui.components;


import java.awt.Color;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.taha.java.actions.HomeControllerImpl;
import com.taha.java.utils.UITool;

public class CustomButton extends JButton {

	private static final long serialVersionUID = 1L;

	private String actionName;
	
	public CustomButton(String value, String icon, String actionName) {
		super();
		this.actionName = actionName;
		JPanel container = new JPanel();
		container.setBackground(UITool.TRASNPARENT_COLOR);
		container.add(new JLabel(new ImageIcon(icon)));
		container.add(new Label(value, UITool.getClassNameFont(), Color.white, 15f, 5));
		add(container);
		setUI(new CustomButtonUI());
		addActionListener(new HomeControllerImpl());
	}

	public String getActionName() {
		return actionName;
	}
}
