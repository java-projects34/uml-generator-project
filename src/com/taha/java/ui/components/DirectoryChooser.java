package com.taha.java.ui.components;

import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.UIManager;
import javax.swing.filechooser.FileFilter;

public class DirectoryChooser extends JFileChooser {

	private static final long serialVersionUID = 1L;

	public DirectoryChooser(String title) {
		super();
		setDialogTitle(title);
		setFileFilter(new DirChooserFilter());
		setFileSelectionMode(JFileChooser. DIRECTORIES_ONLY);
		setCurrentDirectory(new File("../"));
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) { }
	}

	private class DirChooserFilter extends FileFilter {

		@Override
		public boolean accept(File f) {
			return f.isDirectory();
		}

		@Override
		public String getDescription() {
			return "Java Project Directory";
		}
	}


}
