package com.taha.java.ui.components;

public enum MessageType {

	ERROR(0),
	INFO(1);

	private int id;

	private MessageType(int id) {
		this.id = id;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

}
