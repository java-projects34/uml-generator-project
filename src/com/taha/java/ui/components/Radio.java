package com.taha.java.ui.components;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JRadioButton;

public class Radio extends JRadioButton {

	private static final long serialVersionUID = 1L;

	public Radio(String value, Color color) {
		setText(value);
		setUI(new CustomButtonUI());
		setBackground(color);
		addActionListener(new CustomListener());
	}
	
	private class CustomListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			if (e.getSource().getClass() == Radio.class) {
				RadioPanel parent =(RadioPanel) ((Radio)e.getSource()).getParent().getParent();
				if (((Radio)e.getSource()).getText().equals("Light Mode")) {
					parent.setDarkMode(false);
				}
				else {
					parent.setDarkMode(true);
				}
			}
		}
	}
}
