package com.taha.java.ui;

import javax.swing.BoxLayout;
import javax.swing.JPanel;

import com.taha.java.actions.HomeControllerImpl;
import com.taha.java.reflection.ActionButtonsAnnotationEngine;
import com.taha.java.ui.components.RadioPanel;

public class HomeBody extends JPanel {

	private static final long serialVersionUID = 1L;

	public HomeBody() {
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		ActionButtonsAnnotationEngine btnGenerator = new ActionButtonsAnnotationEngine(new HomeControllerImpl());
		add(btnGenerator.getButtons());
		add(new RadioPanel());
	}
	
}
