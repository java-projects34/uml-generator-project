package com.taha.java.ui;

import java.awt.FlowLayout;

import javax.swing.BoxLayout;
import javax.swing.JPanel;

import com.taha.java.utils.UITool;


public class Home extends JPanel {

	private static final long serialVersionUID = 1L;

	public Home() {
		setLayout(new FlowLayout());
		setBackground(UITool.CLASS_COLOR_DARK);
		JPanel container = new JPanel();
		container.setLayout(new BoxLayout(container, BoxLayout.Y_AXIS));
		container.add(new HomeHeader());
		container.add(new HomeBody());
		add(container);
	}

}
